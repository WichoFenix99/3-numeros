package tresnumeros;

/**
 *
 * @author W I C H O
 */
public class TresNumeros {

    /*
      Aplicacion para la colocacion de 3 numeros en la cual uno puede saber cual
      es el mayor, el menor y el de en medio
    */
    public static void main(String[] args) {
      int a = 12;
      int b = 23;
      int c = 99;
        
        if(a>b&&b>c){
          if(c>b){
              System.out.println(b+" "+c+" "+a);
          }
        }else{
            System.out.println(c+" "+b+" "+a);
        }
        
        if(b>c&&c>a){
            if(a>c){
                System.out.println(c+" "+a+" "+b);
            }   
        }else{
          System.out.println(a+" "+c+" "+b);
        }
        
        if(c>a&&a>b){
            if(b>a){
                System.out.println(a+" "+b+" "+c);
            }
        }else{
          System.out.println(b+" "+a+" "+c);
        }
    }
}
